package com.base.project.util

import java.io.IOException

class ApiException(statusCode : Int, message: String) : IOException(message)
class NoInternetException(message: String) : IOException(message)
class SessionExpiredException(message: String) : IOException(message)
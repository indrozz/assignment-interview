package com.base.project.ui

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.AndroidViewModel
import com.base.project.appdata.Repository

abstract class BaseViewModel<T : BaseCallback>(application: Application) : AndroidViewModel(application) {

    protected val repository = Repository(application.applicationContext)
    lateinit var callback: T

    abstract fun onCreate(inState : Bundle?)

    abstract fun onResume()

    abstract fun onPause()

    abstract fun onDestroy()

    abstract fun onSaveInstanceState(outState : Bundle)

    fun onBackPressed() {

    }
}
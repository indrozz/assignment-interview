package com.base.project.appdata

import android.content.Context
import com.base.project.util.changeLanguage
import io.paperdb.Paper

object DataManager {

    val instance get() = this

    var deviceToken: String?
        get() = Paper.book().read(DEVICE_TOKEN, null)
        set(value) {
            Paper.book().write(DEVICE_TOKEN, value)
        }

    val defaultLanguage get() = Paper.book().read(DEFAULT_LANGUAGE, "en")
}
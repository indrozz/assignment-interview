package com.base.project.ui.activity.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\u0003H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0007H&\u00a8\u0006\u000b"}, d2 = {"Lcom/base/project/ui/activity/home/HomeCallback;", "Lcom/base/project/ui/BaseCallback;", "checkAndStartAlarmNotifierService", "", "remainTimeInSec", "", "getSelectedTime", "", "openTimePicker", "setSelectedTime", "time", "app_debug"})
public abstract interface HomeCallback extends com.base.project.ui.BaseCallback {
    
    public abstract void openTimePicker();
    
    public abstract void checkAndStartAlarmNotifierService(int remainTimeInSec);
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getSelectedTime();
    
    public abstract void setSelectedTime(@org.jetbrains.annotations.NotNull()
    java.lang.String time);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public static org.kodein.di.KodeinContext<?> getKodeinContext(com.base.project.ui.activity.home.HomeCallback $this) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public static org.kodein.di.KodeinTrigger getKodeinTrigger(com.base.project.ui.activity.home.HomeCallback $this) {
            return null;
        }
        
        @java.lang.Override()
        public static void dismissAlertDialog(com.base.project.ui.activity.home.HomeCallback $this) {
        }
        
        @java.lang.Override()
        public static void showAlertDialog(com.base.project.ui.activity.home.HomeCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.Object message, int positiveButton, int negativeButton, int neutralButton, boolean isCancelable, int purposeType, @org.jetbrains.annotations.Nullable()
        java.lang.Integer viewResId, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onPositiveButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNegativeButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNeutralButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onCustomAlertDialogCreated) {
        }
        
        @java.lang.Override()
        public static void showMessage(com.base.project.ui.activity.home.HomeCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.String msg) {
        }
    }
}
package com.base.project.databinding;
import com.base.project.R;
import com.base.project.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityHomeBindingImpl extends ActivityHomeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.appcompat.widget.LinearLayoutCompat mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatButton mboundView2;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mHomeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener tvSelectedTimeandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of homeViewModel.selectedTime
            //         is homeViewModel.setSelectedTime((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(tvSelectedTime);
            // localize variables for thread safety
            // homeViewModel != null
            boolean homeViewModelJavaLangObjectNull = false;
            // homeViewModel
            com.base.project.ui.activity.home.HomeViewModel homeViewModel = mHomeViewModel;
            // homeViewModel.selectedTime
            java.lang.String homeViewModelSelectedTime = null;



            homeViewModelJavaLangObjectNull = (homeViewModel) != (null);
            if (homeViewModelJavaLangObjectNull) {




                homeViewModel.setSelectedTime(((java.lang.String) (callbackArg_0)));
            }
        }
    };

    public ActivityHomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ActivityHomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            );
        this.mboundView0 = (androidx.appcompat.widget.LinearLayoutCompat) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatButton) bindings[2];
        this.mboundView2.setTag(null);
        this.tvSelectedTime.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.homeViewModel == variableId) {
            setHomeViewModel((com.base.project.ui.activity.home.HomeViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setHomeViewModel(@Nullable com.base.project.ui.activity.home.HomeViewModel HomeViewModel) {
        this.mHomeViewModel = HomeViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.homeViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.base.project.ui.activity.home.HomeViewModel homeViewModel = mHomeViewModel;
        java.lang.String homeViewModelSelectedTime = null;
        android.view.View.OnClickListener homeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (homeViewModel != null) {
                    // read homeViewModel.selectedTime
                    homeViewModelSelectedTime = homeViewModel.getSelectedTime();
                    // read homeViewModel::onChooseTimeButtonClicked
                    homeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener = (((mHomeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener == null) ? (mHomeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mHomeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener).setValue(homeViewModel));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.mboundView2.setOnClickListener(homeViewModelOnChooseTimeButtonClickedAndroidViewViewOnClickListener);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSelectedTime, homeViewModelSelectedTime);
        }
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.tvSelectedTime, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, tvSelectedTimeandroidTextAttrChanged);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private com.base.project.ui.activity.home.HomeViewModel value;
        public OnClickListenerImpl setValue(com.base.project.ui.activity.home.HomeViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onChooseTimeButtonClicked(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): homeViewModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}
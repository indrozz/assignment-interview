package com.base.project.fcm

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.base.project.R
import com.base.project.appdata.DataManager
import com.base.project.appdata.IS_PUSH_NOTIFICATION_CALL
import com.base.project.appdata.PUSH_NOTIFICATION_DATA
import com.base.project.ui.activity.login.SplashActivity
import com.base.project.util.isAppInInBackground
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

@Suppress("CAST_NEVER_SUCCEEDS")
class FCMService : FirebaseMessagingService() {

    companion object {

        const val CHANNEL_ID = "niit_notification_channel_id"
        const val REQUEST_CODE = 2209
        const val VIBRATION_PATTERN_VALUE = 1000L
        const val COUNTER_TOTAL_TIME = 5000L
        const val COUNTER_INTERVAL = 1000L

    }

    private lateinit var notificationManager: NotificationManager

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        DataManager.deviceToken = token
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        try {
            handlePushWRTFlag(
                PushData(
                    remoteMessage.data["appData"],
                    remoteMessage.data["flag"]!!.toInt(),
                    remoteMessage.data["icon"],
                    remoteMessage.data["id"]!!.toInt(),
                    remoteMessage.data["message"]!!,
                    remoteMessage.data["title"]!!
                )
            )

        } catch (ex: Exception) {
            ex.printStackTrace()
            handlePushWRTFlag(PushData(
                    "",
                    0,
                    "",
                    1,
                    if (remoteMessage.notification != null) remoteMessage.notification!!.body!! else  "Testing Purpose",
                    if (remoteMessage.notification != null) remoteMessage.notification!!.title ?: "Testing Purpose" else  "Testing Purpose"
            ))
        }
    }

    private fun handlePushWRTFlag(pushData: PushData) {

        when (pushData.flag) {
            0 -> handlePush(getNotifyIntent(SplashActivity :: class.java, pushData))
        }
    }

    private fun getNotifyIntent(clazz: Class<out Activity>, pushData: PushData): Intent = Intent(applicationContext, clazz).apply {
        putExtra(IS_PUSH_NOTIFICATION_CALL, true)
        putExtra(PUSH_NOTIFICATION_DATA, pushData)
        setAction(Intent.ACTION_MAIN)
        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//        notifyIntent.addCategory(Intent.CATEGORY_LAUNCHER)
    }

    private fun handlePush(notifyIntent: Intent) {
        Handler(Looper.getMainLooper()).post {
            if (!isAppInInBackground()) {
                dismissNotificationDelay()
            }

            makeNotification(applicationContext, notifyIntent)
        }
    }

    private fun dismissNotificationDelay() {
        object : CountDownTimer(COUNTER_TOTAL_TIME, COUNTER_INTERVAL) {
            override fun onFinish() {
                //do on finish counter
            }

            override fun onTick(millisUntilFinished: Long) {
                clearNotification()
            }
        }
    }

    private fun clearNotification() {
        notificationManager.cancelAll()
    }

    private fun makeNotification(context: Context, notifyIntent: Intent) {

        try {
            val pushData = notifyIntent.getParcelableExtra(PUSH_NOTIFICATION_DATA) as PushData
            val pendingIntent = PendingIntent.getActivity(
                    context,
                    REQUEST_CODE,
                    notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = NotificationChannel(
                        CHANNEL_ID, "notification",
                        NotificationManager.IMPORTANCE_DEFAULT
                )
                notificationManager.createNotificationChannel(notificationChannel)
            }
            notificationManager.notify(
                    pushData.id,
                    NotificationCompat.Builder(context, CHANNEL_ID).apply {
                        setContentTitle(pushData.title)
                        setContentText("" + pushData.message)
                        setAutoCancel(true)
                        setStyle(NotificationCompat.BigTextStyle().bigText(pushData.message))
                        setVibrate(longArrayOf(VIBRATION_PATTERN_VALUE, VIBRATION_PATTERN_VALUE))
                        setContentIntent(pendingIntent)
                        setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        setSmallIcon(R.mipmap.ic_launcher)
                    }.build()
            )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    //private fun getNotificationToneUri(): String? = "android.resource://${getPackageName()}/${R.raw.notification_tone}"

}
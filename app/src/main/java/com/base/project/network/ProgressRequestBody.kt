package com.base.project.network

import android.os.Handler
import android.os.Looper
import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream


/**
 * Developer : Indrajeet Gupta
 * Date : 11/10/18.
 */
class ProgressRequestBody(private val mFile : File, private val onProgressUpdate : (Int)->Unit) : RequestBody() {

    override fun contentType(): MediaType? = MediaType.parse("*/*")

    override fun writeTo(sink: BufferedSink) {
        val fileLength = mFile.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val fileInputStream = FileInputStream(mFile)
        var uploaded: Long = 0

        fileInputStream.use {
            val handler = Handler(Looper.getMainLooper())
            var read = fileInputStream.read(buffer)
            while (read != -1) {

                // update progress on UI thread
                handler.post(ProgressUpdater(uploaded, fileLength))

                uploaded += read.toLong()
                sink.write(buffer, 0, read)
                read = fileInputStream.read(buffer)
            }
        }
    }

    private inner class ProgressUpdater(private val mUploaded: Long, private val mTotal: Long) : Runnable {

        override fun run() {
            onProgressUpdate((100 * mUploaded / mTotal).toInt())
        }
    }

}
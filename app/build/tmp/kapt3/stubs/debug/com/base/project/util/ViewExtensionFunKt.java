package com.base.project.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000@\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\f\u0010\u0005\u001a\u00020\u0004*\u00020\u0002H\u0007\u001a\n\u0010\u0006\u001a\u00020\u0001*\u00020\u0007\u001a\n\u0010\b\u001a\u00020\t*\u00020\u0002\u001a\n\u0010\n\u001a\u00020\t*\u00020\u0002\u001a\u0012\u0010\u000b\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\f\u001a\u00020\r\u001a&\u0010\u000e\u001a\u00020\u0001*\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\t\u001a\u0012\u0010\u0014\u001a\u00020\u0001*\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016\u001a\u0012\u0010\u0017\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0010\u00a8\u0006\u0019"}, d2 = {"changeLanguage", "", "Landroid/content/Context;", "language", "", "getDeviceId", "hideKeyboard", "Landroid/app/Activity;", "isAppInInBackground", "", "isNetworkAvailable", "showKeyboard", "v", "Landroid/view/View;", "snackbarMessage", "message", "", "colorResId", "", "isHide", "tapIt", "listener", "Landroid/animation/AnimatorListenerAdapter;", "toast", "msg", "app_debug"})
public final class ViewExtensionFunKt {
    
    public static final void toast(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$toast, @org.jetbrains.annotations.NotNull()
    java.lang.Object msg) {
    }
    
    public static final void changeLanguage(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$changeLanguage, @org.jetbrains.annotations.NotNull()
    java.lang.String language) {
    }
    
    public static final void showKeyboard(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$showKeyboard, @org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public static final boolean isNetworkAvailable(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$isNetworkAvailable) {
        return false;
    }
    
    public static final boolean isAppInInBackground(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$isAppInInBackground) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"HardwareIds"})
    public static final java.lang.String getDeviceId(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$getDeviceId) {
        return null;
    }
    
    public static final void snackbarMessage(@org.jetbrains.annotations.NotNull()
    android.app.Activity $this$snackbarMessage, @org.jetbrains.annotations.NotNull()
    java.lang.Object message, int colorResId, boolean isHide) {
    }
    
    public static final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    android.app.Activity $this$hideKeyboard) {
    }
    
    public static final void tapIt(@org.jetbrains.annotations.NotNull()
    android.view.View $this$tapIt, @org.jetbrains.annotations.NotNull()
    android.animation.AnimatorListenerAdapter listener) {
    }
}
package com.base.project.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

abstract class BatteryLevelBroadcastReceiver : BroadcastReceiver()

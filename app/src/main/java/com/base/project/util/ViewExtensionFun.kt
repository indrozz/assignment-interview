package com.base.project.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.base.project.R
import com.google.android.material.snackbar.Snackbar
import java.util.*

fun Context.toast(msg : Any) {
    if (msg is Int) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT)
            .show()
    } else {
        Toast.makeText(this, msg.toString(), Toast.LENGTH_SHORT)
            .show()
    }
}

fun Context.changeLanguage(language: String) {

    val locale = Locale(language)
    Locale.setDefault(locale)
    val config = Configuration(resources.configuration)
    config.locale = locale
    resources.updateConfiguration(config, resources.displayMetrics)
}

fun Context.showKeyboard(v: View) {
    try {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun Context.isNetworkAvailable(): Boolean {
    val cm: ConnectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo: NetworkInfo? = cm.activeNetworkInfo
    return (networkInfo != null && networkInfo.isConnectedOrConnecting)
}

fun Context.isAppInInBackground(): Boolean {
    var isInBackground = true
    val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val runningProcesses = am.runningAppProcesses
    for (processInfo in runningProcesses) {
        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
            for (activeProcess in processInfo.pkgList) {
                if (activeProcess == packageName) {
                    isInBackground = false
                }
            }
        }
    }
    return isInBackground
}

@SuppressLint("HardwareIds")
fun Context.getDeviceId(): String {
    return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
}

fun Activity.snackbarMessage(
    message: Any,
    colorResId: Int = R.color.black,
    isHide: Boolean = true
) {

    hideKeyboard()
    val snackbar: Snackbar = if (message is Int) {
        Snackbar.make(
            window.decorView.rootView.findViewById(android.R.id.content),
            message,
            if (isHide) Snackbar.LENGTH_LONG else Snackbar.LENGTH_INDEFINITE
        )
    } else {
        Snackbar.make(
            window.decorView.findViewById(android.R.id.content),
            message.toString(),
            if (isHide) Snackbar.LENGTH_LONG else Snackbar.LENGTH_INDEFINITE
        )
    }
    val sbView: View = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(this, colorResId))
    sbView.layoutParams.width = FrameLayout.LayoutParams.MATCH_PARENT
    snackbar.show()
}

fun Activity.hideKeyboard() {
    try {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (currentFocus != null)
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.tapIt(listener: AnimatorListenerAdapter) {
    animate()
        .scaleX(0.95f)
        .scaleY(0.95f)
        .setDuration(50)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                animate()
                    .scaleX(1f)
                    .scaleY(1f)
                    .setDuration(50)
                    .setListener(listener)
                    .start()
            }
        }).start()
}
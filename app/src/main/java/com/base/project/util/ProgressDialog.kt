package com.base.project.util

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.WindowManager
import com.base.project.R

/**
 * Developer : Indrajeet Gupta
 * Date : 12/1/18.
 */
object ProgressDialog {

    private const val LAYOUT_DIALOG_ID : Int = R.layout.layout_custom_dialog
    private var dialog : Dialog? = null

    private fun initializeDialog(context : Context) {

        dialog = Dialog(context, android.R.style.Theme_Translucent_NoTitleBar)

        val view = View.inflate(
                context,
                LAYOUT_DIALOG_ID,
                null
        )

        dialog!!.setContentView(view)
        dialog!!.window!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        )
        dialog!!.setCancelable(false)
    }

    fun show(context: Context) {

        initializeDialog(context)
        if (!dialog!!.isShowing) {
            dialog!!.show()
        }
    }

    fun dismiss() {

        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog = null
        }
    }
}
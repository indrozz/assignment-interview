package com.base.project.util

import android.content.Context
import android.os.Environment
import android.util.Log
import com.base.project.appdata.LOG_FILE_NAME
import com.base.project.appdata.LOG_FILE
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object AppLog {


    /*
    Log.VERBOSE = 2
    Log.DEBUG = 3
    Log.INFO = 4
    Log.WARN = 5
    Log.ERROR = 6
    Log.ASSERT = 7
     */
    private const val LEVEL = Log.VERBOSE
    private const val IS_TEST = true
    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    fun v(tag: String, message: String) {
        Log.v(tag, message)
        appendLog(tag, message)
    }

    fun d(tag: String, message: String) {
        Log.d(tag, message)
        appendLog(tag, message)
    }

    fun i(tag: String, message: String) {
        Log.i(tag, message)
        appendLog(tag, message)
    }

    fun w(tag: String, message: String) {
        Log.w(tag, message)
        appendLog(tag, message)
    }

    fun e(tag: String, message: String) {
        Log.e(tag, message)
        appendLog(tag, message)
    }

    fun e(tag: String, message: String, exception: Exception) {
        Log.e(tag, message, exception)
        appendLog(tag, message)
    }

    private fun appendLog(
        tag: String,
        text: String
    ) {
        val logFile: File?

        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            val simpleDateFormat = SimpleDateFormat(DateUtils.DATE_TIME_BACKEND_FORMAT, Locale.ENGLISH)
            val dateTime = simpleDateFormat.format(Calendar.getInstance().time)

            val dateFormat = SimpleDateFormat(DateUtils.DATE_TIME_ONLY_DATE_FORMAT_4, Locale.ENGLISH)
            val date = dateFormat.format(Calendar.getInstance().time)

            //get external storage path....
            val folderFileObject = FileUtils.getFolderOnExternalDirectory(
                LOG_FILE,
                context
            )

            logFile = File(folderFileObject.absolutePath + "/" + date + "_" + LOG_FILE_NAME)

            if (!logFile.exists()) {
                var bufferedWriter: BufferedWriter? = null
                try {
                    logFile.createNewFile()
                    //write device info first time in db...
                    bufferedWriter = BufferedWriter(FileWriter(logFile, true))
                    bufferedWriter.append("DEVICE INFO")
                    bufferedWriter.append(" ")
                    bufferedWriter.append(context.getDeviceId())
                    bufferedWriter.newLine()
                    bufferedWriter.append("USER ID")
                    bufferedWriter.append(" ")
                    //bufferedWriter.append(DataManager().firstName + " " + DataManager().lastName)
                    bufferedWriter.newLine()

                } catch (ioException: IOException) {
                    ioException.printStackTrace()
                } finally {
                    try {
                        bufferedWriter!!.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            var bufferedWriter: BufferedWriter? = null
            try {
                bufferedWriter = BufferedWriter(FileWriter(logFile, true))
                bufferedWriter.append(dateTime)
                bufferedWriter.append(" ")
                bufferedWriter.append(tag)
                bufferedWriter.append(" ")
                bufferedWriter.append(text)
                bufferedWriter.newLine()
            } catch (ioException: IOException) {
                ioException.printStackTrace()
            } finally {
                try {
                    bufferedWriter!!.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
}

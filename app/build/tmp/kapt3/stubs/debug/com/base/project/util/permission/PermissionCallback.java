package com.base.project.util.permission;

import java.lang.System;

/**
 * Developer : Indrajeet Gupta
 * Date : 14/07/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J%\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u0007H&\u00a2\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u0003H&\u00a8\u0006\u000b"}, d2 = {"Lcom/base/project/util/permission/PermissionCallback;", "", "onPermissionDenied", "", "requestCode", "", "perms", "", "", "(I[Ljava/lang/String;)V", "onPermissionGranted", "app_debug"})
public abstract interface PermissionCallback {
    
    public abstract void onPermissionGranted();
    
    public abstract void onPermissionDenied(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] perms);
}
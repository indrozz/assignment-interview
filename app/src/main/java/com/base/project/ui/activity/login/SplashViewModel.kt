package com.base.project.ui.activity.login

import android.app.Application
import android.os.Bundle
import com.base.project.ui.BaseViewModel
import com.base.project.util.ApiException
import com.base.project.util.Coroutines
import com.base.project.util.NoInternetException
import com.base.project.util.SessionExpiredException

class SplashViewModel(application: Application) : BaseViewModel<SplashCallback>(application) {

    override fun onResume() {

    }

    override fun onPause() {

    }

    override fun onDestroy() {

    }

    override fun onCreate(inState: Bundle?) {
        callback.initFingerPrintAuth (
            {
                callback.gotoHomeActivity()
            },
            {
                callback.showMessage(it)
            }
        )

    }

    override fun onSaveInstanceState(outState: Bundle) {

    }
}
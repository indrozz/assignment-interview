package com.base.project.network

import com.base.project.model.BaseModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Developer : indrozz
 * Date : 06/03/18.
 */
interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ) : Response<BaseModel<Any>>
}
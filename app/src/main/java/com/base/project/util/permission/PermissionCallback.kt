package com.base.project.util.permission

/**
 * Developer : Indrajeet Gupta
 * Date : 14/07/18.
 */
interface PermissionCallback {
    fun onPermissionGranted()
    fun onPermissionDenied(requestCode: Int, perms: Array<out String>)
}
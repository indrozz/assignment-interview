package com.base.project.appdata;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"DEFAULT_LANGUAGE", "", "DEVICE_TOKEN", "IS_PUSH_NOTIFICATION_CALL", "KEY_REMAINING_TIME_IN_SEC", "KEY_SELECTED_TIME", "LOG_FILE", "LOG_FILE_NAME", "PUSH_NOTIFICATION_DATA", "app_debug"})
public final class ConstantKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEFAULT_LANGUAGE = "default_language";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOG_FILE = "Log File";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOG_FILE_NAME = "baseProject-log.txt";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVICE_TOKEN = "device_token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_PUSH_NOTIFICATION_CALL = "is_push_notification_call";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PUSH_NOTIFICATION_DATA = "push_notification_data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_REMAINING_TIME_IN_SEC = "time_in_seconds";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_SELECTED_TIME = "selected_time";
}
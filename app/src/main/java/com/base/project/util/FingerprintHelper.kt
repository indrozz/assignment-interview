package com.base.project.util

import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import com.base.project.R
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey


object FingerprintHelper : FingerprintManager.AuthenticationCallback() {

    private const val ANDROID_KEY_STORE = "AndroidKeyStore";
    private const val KEY_STORE_ALIAS = "AndroidKey";

    private lateinit var context: Context
    private var onVerified: (() -> Unit)? = null
    private var onError: ((String) -> Unit)? = null
    private var cipher: Cipher? = null
    private lateinit var keyStore: KeyStore

    private fun initCipher() : Boolean {

        try {

            cipher  = Cipher.getInstance(
                KeyProperties.KEY_ALGORITHM_AES + "/"
                        + KeyProperties.BLOCK_MODE_CBC + "/"
                        + KeyProperties.ENCRYPTION_PADDING_PKCS7
            )
            val key: SecretKey
            keyStore = KeyStore.getInstance(ANDROID_KEY_STORE)
            keyStore.load(null)
            key = keyStore.getKey(KEY_STORE_ALIAS, null) as SecretKey
            cipher!!.init(Cipher.ENCRYPT_MODE, key)

        } catch (e: Exception) {
            onError ?. invoke(e.message ?: "Failed")

        } finally {
            return cipher != null
        }
    }

    private fun generateKey() {
        val keyGenerator: KeyGenerator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES,
            ANDROID_KEY_STORE
        )
        keyGenerator.init(
            KeyGenParameterSpec.Builder(
                KEY_STORE_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
            )
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setUserAuthenticationRequired(true)
                .setEncryptionPaddings(
                    KeyProperties.ENCRYPTION_PADDING_PKCS7
                )
                .build()
        )
        keyGenerator.generateKey()
    }

    fun init(context: Context): FingerprintHelper {
        this.context = context
        return this
    }

    fun with(onVerified: () -> Unit, onError: (String) -> Unit) : FingerprintHelper {
        this.onVerified = onVerified
        this.onError = onError

        return this
    }

    fun startAuth(
        fm: FingerprintManager
    ) {
        if (initCipher()) {
            fm.authenticate(
                FingerprintManager.CryptoObject(cipher!!),
                CancellationSignal(),
                0,
                this,
                null
            )
        }
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        onVerified ?. invoke()
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        onError ?. invoke(context.getString(R.string.error_not_recognized))
    }
}
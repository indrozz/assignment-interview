package com.base.project.util

import android.content.Context
import com.base.project.appdata.LOG_FILE_NAME
import com.base.project.appdata.LOG_FILE
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object FileUtils {

    private const val AVATAR_DIR = "avatar"
    private const val AVATAR_FILE_NAME = "avatar.jpg"

    fun getFolderOnExternalDirectory(folderName: String, context: Context): File {
        var doesFolderExists: Boolean
        var folderFileObject: File?
        val externalFilesDirectory = context.getExternalFilesDir(null as String?)
        if (folderName.isNotEmpty() && folderName.trim().isNotEmpty()) {
            folderFileObject = File(externalFilesDirectory, folderName)
            doesFolderExists = folderFileObject.exists()
            if (!doesFolderExists) {
                doesFolderExists = folderFileObject.mkdirs()
                if (!doesFolderExists) {
                    folderFileObject = externalFilesDirectory
                }
            }
        } else {
            folderFileObject = externalFilesDirectory
        }

        return folderFileObject!!
    }

    fun getLogFile(context: Context): File {
        val folderFileObject = getFolderOnExternalDirectory(LOG_FILE, context)
        val dateFormat = SimpleDateFormat(DateUtils.DATE_TIME_ONLY_DATE_FORMAT_4, Locale.ENGLISH)
        val date = dateFormat.format(Calendar.getInstance().time)
        return File(folderFileObject.absolutePath + "/" + date + "_" + LOG_FILE_NAME)
    }
}
package com.base.project.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B$\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\u000b\u0010\u0007\u001a\u00078\u0000\u00a2\u0006\u0002\b\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\u0013\u0010\u0013\u001a\u00078\u0000\u00a2\u0006\u0002\b\bH\u00c6\u0003\u00a2\u0006\u0002\u0010\rJ7\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\r\b\u0002\u0010\u0007\u001a\u00078\u0000\u00a2\u0006\u0002\b\bH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0015J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0004H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001R\u0016\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001d\u0010\u0007\u001a\u00078\u0000\u00a2\u0006\u0002\b\b8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\rR\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001b"}, d2 = {"Lcom/base/project/model/BaseModel;", "T", "", "statusCode", "", "message", "", "responseData", "Lkotlinx/android/parcel/RawValue;", "(ILjava/lang/String;Ljava/lang/Object;)V", "getMessage", "()Ljava/lang/String;", "getResponseData", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getStatusCode", "()I", "component1", "component2", "component3", "copy", "(ILjava/lang/String;Ljava/lang/Object;)Lcom/base/project/model/BaseModel;", "equals", "", "other", "hashCode", "toString", "app_debug"})
public final class BaseModel<T extends java.lang.Object> {
    @com.google.gson.annotations.SerializedName(value = "statusCode")
    private final int statusCode = 0;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "message")
    private final java.lang.String message = null;
    @com.google.gson.annotations.SerializedName(value = "responseData")
    private final T responseData = null;
    
    public final int getStatusCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final T getResponseData() {
        return null;
    }
    
    public BaseModel(int statusCode, @org.jetbrains.annotations.NotNull()
    java.lang.String message, T responseData) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    public final T component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.base.project.model.BaseModel<T> copy(int statusCode, @org.jetbrains.annotations.NotNull()
    java.lang.String message, T responseData) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}
package com.base.project.ui.activity.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u00042\u00020\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016J\"\u0010\u000f\u001a\u00020\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\fH\u0016J\b\u0010\u0014\u001a\u00020\nH\u0016J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u000eH\u0016J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\u000eH\u0016\u00a8\u0006\u0019"}, d2 = {"Lcom/base/project/ui/activity/home/HomeActivity;", "Lcom/base/project/ui/activity/BaseActivity;", "Lcom/base/project/databinding/ActivityHomeBinding;", "Lcom/base/project/ui/activity/home/HomeViewModel;", "Lcom/base/project/ui/activity/home/HomeCallback;", "Landroid/app/TimePickerDialog$OnTimeSetListener;", "()V", "bindViewModel", "", "checkAndStartAlarmNotifierService", "", "remainTimeInSec", "", "getSelectedTime", "", "onTimeSet", "view", "Landroid/widget/TimePicker;", "hourOfDay", "minute", "openTimePicker", "setSelectedTime", "time", "showMessage", "msg", "app_debug"})
public final class HomeActivity extends com.base.project.ui.activity.BaseActivity<com.base.project.databinding.ActivityHomeBinding, com.base.project.ui.activity.home.HomeViewModel> implements com.base.project.ui.activity.home.HomeCallback, android.app.TimePickerDialog.OnTimeSetListener {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public boolean bindViewModel() {
        return false;
    }
    
    @java.lang.Override()
    public void openTimePicker() {
    }
    
    @java.lang.Override()
    public void checkAndStartAlarmNotifierService(int remainTimeInSec) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getSelectedTime() {
        return null;
    }
    
    @java.lang.Override()
    public void setSelectedTime(@org.jetbrains.annotations.NotNull()
    java.lang.String time) {
    }
    
    @java.lang.Override()
    public void onTimeSet(@org.jetbrains.annotations.Nullable()
    android.widget.TimePicker view, int hourOfDay, int minute) {
    }
    
    @java.lang.Override()
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    public HomeActivity() {
        super(0, null);
    }
}
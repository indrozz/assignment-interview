package com.base.project.util.permission

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.base.project.R

/**
 * Developer : Indrajeet Gupta
 * Date : 14/07/18.
 */
object Permission {

    private const val REQUEST_PERMISSION_CODE = 999

    private var callback: PermissionCallback? = null
    private var permissionMessage: Int = R.string.message_permission
    private var alertDialog: AlertDialog? = null

    fun hasPermission(activity: Activity, perms: Array<String>) : Boolean {
        for (perm in perms) {
            if (ContextCompat.checkSelfPermission(activity, perm) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun requestPermission(activity: Activity, permissionMessage: Int, perms: Array<out String>) {
        Permission.permissionMessage = permissionMessage
        for (perm in perms) {
            if (ContextCompat.checkSelfPermission(activity, perm) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, perms, REQUEST_PERMISSION_CODE)
            }
        }
    }

    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != REQUEST_PERMISSION_CODE) {
            return
        }
        var granted = true
        var i = 0
        for (perm in permissions) {
            granted = granted and (grantResults[i++] == PackageManager.PERMISSION_GRANTED)
        }
        if (permissions.isNotEmpty() && granted) {
            if (callback != null) {
                callback!!.onPermissionGranted()
            }
        } else {
            callback ?: return
            callback!!.onPermissionDenied(requestCode, permissions)
        }
    }

    fun setCallback(callback: PermissionCallback): Permission {
        Permission.callback = callback
        return this
    }

    fun showPermissionAlertDialog(context: Context, requestCode: Int, perms: Array<out String>) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(permissionMessage)
            .setPositiveButton(
                "Ok"
            ) { _, _ ->
                if (callback != null) {
                    callback!!.onPermissionDenied(requestCode, perms)
                }
                alertDialog!!.dismiss()
                alertDialog = null
            }
        alertDialog = builder.create()
        alertDialog!!.show()
    }
}
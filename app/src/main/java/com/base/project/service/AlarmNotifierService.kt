package com.base.project.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.os.BatteryManager
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import com.base.project.R
import com.base.project.appdata.KEY_REMAINING_TIME_IN_SEC
import com.base.project.appdata.KEY_SELECTED_TIME
import com.base.project.fcm.FCMService
import com.base.project.receiver.BatteryLevelBroadcastReceiver
import com.base.project.ui.activity.login.SplashActivity


class AlarmNotifierService : Service() {

    companion object {
        const val LOW_LEVEL = 15
    }

    private var level = 100

    private val batteryBroadcastReceiver = object : BatteryLevelBroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            level = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
        }
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) {
            stopSelf()

        } else {
            registerReceiver(batteryBroadcastReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
            object : CountDownTimer(intent.getIntExtra(KEY_REMAINING_TIME_IN_SEC, 0) * 1000L, 1000) {
                override fun onTick(millisUntilFinished: Long) {}

                override fun onFinish() {
                    if (level > LOW_LEVEL) {
                        showNotification()
                    }
                    unregisterReceiver(batteryBroadcastReceiver)
                    setSelectedTime(getString(R.string.text_default_time))
                    stopSelf()
                }
            }.start()
        }
        return START_NOT_STICKY
    }

    private fun showNotification() {
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                FCMService.CHANNEL_ID, "notification",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val pendingIntent = PendingIntent.getActivity(
            applicationContext,
            FCMService.REQUEST_CODE,
            Intent(applicationContext, SplashActivity :: class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        notificationManager.notify(
            1,
            NotificationCompat.Builder(applicationContext, FCMService.CHANNEL_ID).apply {
                setContentTitle(getString(R.string.text_alarm_alert))
                setContentText(getString(R.string.text_alarm_started))
                setAutoCancel(true)
                setStyle(NotificationCompat.BigTextStyle().bigText(getString(R.string.text_alarm_started)))
                setVibrate(longArrayOf(
                    FCMService.VIBRATION_PATTERN_VALUE,
                    FCMService.VIBRATION_PATTERN_VALUE
                ))
                setContentIntent(pendingIntent)
                setLargeIcon(BitmapFactory.decodeResource(application.getResources(), R.mipmap.ic_launcher))
                setSmallIcon(R.mipmap.ic_launcher)
            }.build()
        )
    }

    private fun setSelectedTime(time: String) {
        val sharedPreferences = getSharedPreferences("db", AppCompatActivity.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(KEY_SELECTED_TIME, time)
        editor.apply()
    }

}

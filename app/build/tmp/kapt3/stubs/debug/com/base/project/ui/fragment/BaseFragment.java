package com.base.project.ui.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\u0010\b\u0001\u0010\u0003*\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u00042\u00020\u00062\u00020\u0005B%\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0016\u0010\t\u001a\u0012\u0012\u000e\b\u0001\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u00040\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\"\u001a\u00020#H&J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010)2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\b\u0010,\u001a\u00020-H\u0016J\b\u0010.\u001a\u00020-H\u0016J\b\u0010/\u001a\u00020-H\u0016J\u001a\u00100\u001a\u00020-2\u0006\u00101\u001a\u00020%2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016R\u001c\u0010\f\u001a\u00028\u0000X\u0084.\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00198VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001e\u0010\t\u001a\u0012\u0012\u000e\b\u0001\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u00040\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001c\u001a\u00028\u0001X\u0084.\u00a2\u0006\u0010\n\u0002\u0010!\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 \u00a8\u00062"}, d2 = {"Lcom/base/project/ui/fragment/BaseFragment;", "T", "Landroidx/databinding/ViewDataBinding;", "U", "Lcom/base/project/ui/BaseViewModel;", "Lcom/base/project/ui/BaseCallback;", "Landroidx/fragment/app/Fragment;", "layoutResId", "", "modelClass", "Ljava/lang/Class;", "(ILjava/lang/Class;)V", "binding", "getBinding", "()Landroidx/databinding/ViewDataBinding;", "setBinding", "(Landroidx/databinding/ViewDataBinding;)V", "Landroidx/databinding/ViewDataBinding;", "mAlertDialog", "Landroidx/appcompat/app/AlertDialog;", "getMAlertDialog", "()Landroidx/appcompat/app/AlertDialog;", "setMAlertDialog", "(Landroidx/appcompat/app/AlertDialog;)V", "mContext", "Landroid/content/Context;", "getMContext", "()Landroid/content/Context;", "viewModel", "getViewModel", "()Lcom/base/project/ui/BaseViewModel;", "setViewModel", "(Lcom/base/project/ui/BaseViewModel;)V", "Lcom/base/project/ui/BaseViewModel;", "bindViewModel", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "", "onPause", "onResume", "onViewCreated", "view", "app_debug"})
public abstract class BaseFragment<T extends androidx.databinding.ViewDataBinding, U extends com.base.project.ui.BaseViewModel<? extends com.base.project.ui.BaseCallback>> extends androidx.fragment.app.Fragment implements com.base.project.ui.BaseCallback {
    @org.jetbrains.annotations.Nullable()
    private androidx.appcompat.app.AlertDialog mAlertDialog;
    @org.jetbrains.annotations.NotNull()
    protected T binding;
    @org.jetbrains.annotations.NotNull()
    protected U viewModel;
    private final int layoutResId = 0;
    private final java.lang.Class<? extends com.base.project.ui.BaseViewModel<? extends com.base.project.ui.BaseCallback>> modelClass = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.content.Context getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public androidx.appcompat.app.AlertDialog getMAlertDialog() {
        return null;
    }
    
    @java.lang.Override()
    public void setMAlertDialog(@org.jetbrains.annotations.Nullable()
    androidx.appcompat.app.AlertDialog p0) {
    }
    
    public abstract boolean bindViewModel();
    
    @org.jetbrains.annotations.NotNull()
    protected final T getBinding() {
        return null;
    }
    
    protected final void setBinding(@org.jetbrains.annotations.NotNull()
    T p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final U getViewModel() {
        return null;
    }
    
    protected final void setViewModel(@org.jetbrains.annotations.NotNull()
    U p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    public BaseFragment(int layoutResId, @org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends com.base.project.ui.BaseViewModel<? extends com.base.project.ui.BaseCallback>> modelClass) {
        super();
    }
    
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.Object message, int positiveButton, int negativeButton, int neutralButton, boolean isCancelable, int purposeType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer viewResId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onPositiveButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNegativeButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNeutralButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onCustomAlertDialogCreated) {
    }
    
    public void dismissAlertDialog() {
    }
    
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.kodein.di.KodeinContext<?> getKodeinContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public org.kodein.di.KodeinTrigger getKodeinTrigger() {
        return null;
    }
}
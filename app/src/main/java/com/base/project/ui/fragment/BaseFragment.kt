package com.base.project.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.base.project.ui.BaseCallback
import com.base.project.ui.BaseViewModel
import org.kodein.di.android.kodein

abstract class BaseFragment<T : ViewDataBinding, U : BaseViewModel<out BaseCallback>>(
    private val layoutResId : Int,
    private val modelClass: Class<out BaseViewModel<out BaseCallback>>

) : Fragment(), BaseCallback {

    override val mContext: Context
        get() = requireActivity()

    override var mAlertDialog: AlertDialog? = null

    abstract fun bindViewModel() : Boolean

    protected lateinit var binding : T
    protected lateinit var viewModel : U

    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {

        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
        viewModel = ViewModelProvider(this).get(modelClass) as U
        bindViewModel()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.onDestroy()
    }

}
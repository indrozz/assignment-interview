package com.base.project.util

import android.net.Uri
import android.webkit.MimeTypeMap
import java.io.File
import java.util.*

object Util {

    fun getMimeType(file : File) : String? {

        var mimeType : String? = null
        val extension : String?
                = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())

        if (extension != null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }

        return mimeType
    }

    fun getRoundOffDecimalValue(value: Double, decimalPlace : Int) : String {
        val s = value.toString().split(".")
        return if (s[1] == "0") s[0] else String.format(Locale.US, "%.${decimalPlace}f", value)
    }

    fun getRoundOffDecimalValue(value: String, decimalPlace : Int) : String {
        val s = value.split(".")
        return if (s[1].toDouble() == 0.0) s[0] else String.format(Locale.US, "%.${decimalPlace}f", value.toDouble())
    }
}
package com.base.project.ui.activity.home

import android.app.Application
import android.os.Bundle
import android.view.View
import com.base.project.R
import com.base.project.ui.BaseCallback
import com.base.project.ui.BaseViewModel
import java.util.*

class HomeViewModel(application: Application) : BaseViewModel<HomeCallback>(application) {

    var selectedTime = application.getString(R.string.text_default_time)

    private fun getRemainingTimeInSeconds(selectedTimeInMin: Int): Int {
        val currCal = Calendar.getInstance()
        val currTimeInMin = currCal.get(Calendar.HOUR_OF_DAY) * 60 + currCal.get(Calendar.MINUTE)

        return ((selectedTimeInMin - currTimeInMin) * 60)
    }

    fun onChooseTimeButtonClicked(view : View) {
        callback.openTimePicker()
    }

    fun onTimeSet(hour : Int, minute : Int) {
        val remainingTimeInSec = getRemainingTimeInSeconds(hour * 60 + minute)
        if (remainingTimeInSec <= 0) {
            callback.showMessage(callback.mContext.getString(R.string.error_selected_time_greater_current_time))

        } else {
            callback.checkAndStartAlarmNotifierService(remainingTimeInSec)
            callback.setSelectedTime(selectedTime)
        }
    }

    override fun onCreate(inState: Bundle?) {

    }

    override fun onResume() {
        selectedTime = callback.getSelectedTime()
    }

    override fun onPause() {

    }

    override fun onDestroy() {

    }

    override fun onSaveInstanceState(outState: Bundle) {

    }
}
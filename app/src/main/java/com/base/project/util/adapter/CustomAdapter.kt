package com.base.project.util.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Developer : indrozz
 * Date : 09/03/18.
 */
abstract class CustomAdapter<T>(
        private val layoutResId : Int,
        private var mList : ArrayList<T>
) : androidx.recyclerview.widget.RecyclerView.Adapter<ItemViewHolder>() {

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        viewBinder(holder, mList[position], position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view : View = LayoutInflater.from(parent.context)
                .inflate(layoutResId, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int = mList.size

    //=============================== unoverriden methods ==================================

    fun setList(list : ArrayList<T>) {
        mList = list
        notifyDataSetChanged()
    }

    fun getList() : ArrayList<T> = this.mList

    fun addMoreList(list : ArrayList<T>, isClear: Boolean = false) {
        if (isClear) {
            mList.clear()
        }
        mList.addAll(list)
        notifyDataSetChanged()
    }

    fun clearList() {
        mList.clear()
        notifyDataSetChanged()
    }

    fun addItem(item : T) {
        mList.add(item)
        notifyDataSetChanged()
    }

    fun removeLastItem() {
        if (mList.size > 0) {
            mList[mList.size - 1]
        }
        notifyDataSetChanged()
    }

    fun removeItem(item : T) {
        if (mList.isNotEmpty()) {
            mList.remove(item)
            notifyDataSetChanged()
        }
    }

    fun isListEmpty() : Boolean = mList.isEmpty()

    //================================ abstract methods =====================================

    abstract fun viewBinder(holder: ItemViewHolder, objects : T, position: Int)

    //==================================== private members ========================================
}

class ItemViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view)

package com.base.project.appdata;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0019\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R(\u0010\t\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\n\u0010\u0007\"\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u00008F\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/base/project/appdata/DataManager;", "", "()V", "defaultLanguage", "", "kotlin.jvm.PlatformType", "getDefaultLanguage", "()Ljava/lang/String;", "value", "deviceToken", "getDeviceToken", "setDeviceToken", "(Ljava/lang/String;)V", "instance", "getInstance", "()Lcom/base/project/appdata/DataManager;", "app_debug"})
public final class DataManager {
    public static final com.base.project.appdata.DataManager INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.base.project.appdata.DataManager getInstance() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDeviceToken() {
        return null;
    }
    
    public final void setDeviceToken(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    public final java.lang.String getDefaultLanguage() {
        return null;
    }
    
    private DataManager() {
        super();
    }
}
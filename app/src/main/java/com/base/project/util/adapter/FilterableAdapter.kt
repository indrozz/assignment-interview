package com.base.project.util.adapter

import android.widget.Filter
import android.widget.Filterable

/**
 * Developer : indrozz
 * Date : 07/02/18.
 */
abstract class FilterableAdapter<T>(
    private var mList : ArrayList<T>,
    private val layoutResId : Int
) : CustomAdapter<T>(layoutResId, mList), Filterable {

    abstract fun performFilteration(filterationText: String): ArrayList<T>

    override fun getItemCount(): Int = mList.size

    override fun getFilter(): Filter {
        if (customFilter != null) {
            mList.clear()
            mList.addAll(tempList)
        }
        if (customFilter == null)
            customFilter = CustomFilter()

        return customFilter as CustomFilter
    }

    //====================================== private members =======================================

    private var customFilter : CustomFilter? = null
    private var tempList : ArrayList<T>

    init {
        tempList = ArrayList<T>(mList)
    }

    private inner class CustomFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {

            val fResult : FilterResults = FilterResults()
            var filterList : ArrayList<T> = ArrayList()

            if (constraint != null && constraint.length > 0) {
                filterList = performFilteration(constraint.toString())
                fResult.values = filterList
            } else {
                fResult.values = mList
            }

            fResult.count = filterList.size
            return fResult
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mList.clear()
            if (!constraint!!.isEmpty() || results!!.count != 0) {
                mList.addAll(results!!.values as ArrayList<T>)
            } else {
                mList.addAll(tempList)
            }
            notifyDataSetChanged()
        }
    }
}

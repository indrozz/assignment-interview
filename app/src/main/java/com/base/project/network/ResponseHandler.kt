package com.base.project.network

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.base.project.R
import com.base.project.model.BaseModel
import com.base.project.util.ApiException
import com.base.project.util.NoInternetException
import com.base.project.util.SessionExpiredException
import com.base.project.util.isNetworkAvailable
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

object ResponseHandler {

    suspend fun<T> handle(context : Context, call : suspend () -> Response<BaseModel<T>>) : T {

        if (!context.isNetworkAvailable()) {
            throw NoInternetException(context.getString(R.string.text_no_internet))
        }

        val response = call.invoke()
        if (response.isSuccessful) {
            response.body().let {
                when (it!!.statusCode) {
                    200 -> return it.responseData
                    401 -> throw SessionExpiredException(it.message)
                    else -> throw ApiException(it.statusCode, it.message)
                }
            }
        } else {
            val error = response.errorBody()?.toString()
            val message = StringBuilder()
            error?.let{
                try{
                    message.append(JSONObject(it).getString("message"))
                }catch(e: JSONException){
                    message.append("\n")
                }
            }
            throw ApiException(response.code(), message.toString())
        }
    }

}

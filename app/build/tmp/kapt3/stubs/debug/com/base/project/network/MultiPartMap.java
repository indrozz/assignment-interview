package com.base.project.network;

import java.lang.System;

/**
 * Developer : indrozz
 * Date : 07/03/18.
 */
@kotlin.Suppress(names = {"NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u001e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u00042\u00020\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\bJ\u0016\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\fJ\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\fH\u0002J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0002H\u0002\u00a8\u0006\u0011"}, d2 = {"Lcom/base/project/network/MultiPartMap;", "Ljava/util/HashMap;", "", "Lokhttp3/RequestBody;", "Lkotlin/collections/HashMap;", "Ljava/io/Serializable;", "()V", "add", "", "key", "value", "addFile", "Ljava/io/File;", "file", "getFileRequestBody", "getSimpleRequestBody", "s", "app_debug"})
public final class MultiPartMap extends java.util.HashMap<java.lang.String, okhttp3.RequestBody> implements java.io.Serializable {
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object add(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Object value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.io.File addFile(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.io.File file) {
        return null;
    }
    
    private final okhttp3.RequestBody getFileRequestBody(java.io.File file) {
        return null;
    }
    
    private final okhttp3.RequestBody getSimpleRequestBody(java.lang.String s) {
        return null;
    }
    
    public MultiPartMap() {
        super(0, 0.0F);
    }
    
    @java.lang.Override()
    public boolean containsValue(okhttp3.RequestBody p0) {
        return false;
    }
    
    @java.lang.Override()
    public final boolean containsValue(java.lang.Object p0) {
        return false;
    }
    
    @java.lang.Override()
    public okhttp3.RequestBody getOrDefault(java.lang.String p0, okhttp3.RequestBody p1) {
        return null;
    }
    
    @java.lang.Override()
    public final java.lang.Object getOrDefault(java.lang.Object p0, java.lang.Object p1) {
        return null;
    }
    
    @java.lang.Override()
    public boolean containsKey(java.lang.String p0) {
        return false;
    }
    
    @java.lang.Override()
    public final boolean containsKey(java.lang.Object p0) {
        return false;
    }
    
    @java.lang.Override()
    public okhttp3.RequestBody get(java.lang.String p0) {
        return null;
    }
    
    @java.lang.Override()
    public final java.lang.Object get(java.lang.Object p0) {
        return null;
    }
    
    @java.lang.Override()
    public okhttp3.RequestBody remove(java.lang.String p0) {
        return null;
    }
    
    @java.lang.Override()
    public final java.lang.Object remove(java.lang.Object p0) {
        return null;
    }
    
    @java.lang.Override()
    public boolean remove(java.lang.String p0, okhttp3.RequestBody p1) {
        return false;
    }
    
    @java.lang.Override()
    public final boolean remove(java.lang.Object p0, java.lang.Object p1) {
        return false;
    }
    
    @java.lang.Override()
    public java.util.Set getEntries() {
        return null;
    }
    
    @java.lang.Override()
    public final java.util.Set<java.util.Map.Entry<java.lang.String, okhttp3.RequestBody>> entrySet() {
        return null;
    }
    
    @java.lang.Override()
    public java.util.Set getKeys() {
        return null;
    }
    
    @java.lang.Override()
    public final java.util.Set<java.lang.String> keySet() {
        return null;
    }
    
    @java.lang.Override()
    public java.util.Collection getValues() {
        return null;
    }
    
    @java.lang.Override()
    public final java.util.Collection<okhttp3.RequestBody> values() {
        return null;
    }
    
    @java.lang.Override()
    public int getSize() {
        return 0;
    }
    
    @java.lang.Override()
    public final int size() {
        return 0;
    }
}
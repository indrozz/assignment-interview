package com.base.project.network

import com.base.project.util.Util.getMimeType
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.io.Serializable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
/**
 * Developer : indrozz
 * Date : 07/03/18.
 */
class MultiPartMap : HashMap<String, RequestBody>(), Serializable {

    fun add(key : String, value : Any) : Any {

        super.put(key, getSimpleRequestBody(value.toString()))
        return value.toString()
    }

    fun addFile(key : String, file : File) : File {

        super.put(key + "\"; filename=\"" + file.name, getFileRequestBody(file))
        return file
    }

    private fun getFileRequestBody(file: File): RequestBody =
            RequestBody.create(MediaType.parse(getMimeType(file)), file)


    private fun getSimpleRequestBody(s: String): RequestBody =
            RequestBody.create(MediaType.parse("text/plain"), s)

}
package com.base.project.ui

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.base.project.R
import com.base.project.util.toast
import org.kodein.di.KodeinAware

interface BaseCallback : KodeinAware {

    val mContext : Context
    var mAlertDialog : AlertDialog?

    fun showAlertDialog(
        message: Any = "",
        positiveButton: Int = R.string.text_ok,
        negativeButton: Int = R.string.empty,
        neutralButton: Int = R.string.empty,
        isCancelable: Boolean = false,
        purposeType: Int = -1,
        viewResId: Int? = null,
        onPositiveButtonClicked : (Int)->Unit = {},
        onNegativeButtonClicked : (Int)->Unit = {},
        onNeutralButtonClicked : (Int)->Unit = {},
        onCustomAlertDialogCreated : (View)->Unit = {}
    ) {

        val alertDialogBuilder = AlertDialog.Builder(mContext)

        if (viewResId == null) {
            if (message is Int) {
                alertDialogBuilder.setMessage(message)
            } else {
                alertDialogBuilder.setMessage(message.toString())
            }

            alertDialogBuilder.setPositiveButton(
                positiveButton
            ) { _, _ ->
                onPositiveButtonClicked(purposeType)
            }
                .setCancelable(isCancelable)
            if (mContext.getString(negativeButton).isNotEmpty()) {
                alertDialogBuilder.setNegativeButton(
                    negativeButton
                ) { _, _ ->
                    onNegativeButtonClicked(purposeType)
                }
            }
            if (mContext.getString(neutralButton).isNotEmpty()) {
                alertDialogBuilder.setNeutralButton(
                    negativeButton
                ) { _, _ ->
                    onNeutralButtonClicked(purposeType)
                }
            }
        } else {
            val itemView = View.inflate(mContext, viewResId, null)
            alertDialogBuilder.setView(itemView)
            onCustomAlertDialogCreated(itemView)
        }

        mAlertDialog = alertDialogBuilder.create()
        mAlertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        mAlertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mAlertDialog!!.show()
    }

    fun dismissAlertDialog() {
        mAlertDialog ?: return
        mAlertDialog!!.dismiss()
        mAlertDialog = null
    }

    fun showMessage(msg: String) {
        mContext.toast(msg)
    }
}
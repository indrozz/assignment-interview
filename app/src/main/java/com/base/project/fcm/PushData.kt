package com.base.project.fcm


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

data class PushData(
        @SerializedName("data")
        val data: @RawValue Any?,
        @SerializedName("flag")
        val flag: Int,
        @SerializedName("icon")
        val icon: String?,
        @SerializedName("id")
        val id: Int,
        @SerializedName("message")
        val message: String,
        @SerializedName("title")
        val title: String
) : Parcelable {
        constructor(parcel: Parcel) : this(
                TODO("data"),
                parcel.readInt(),
                parcel.readString(),
                parcel.readInt(),
                parcel.readString()!!,
                parcel.readString()!!
        ) {
        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {
                TODO("Not yet implemented")
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<PushData> {
                override fun createFromParcel(parcel: Parcel): PushData {
                        return PushData(parcel)
                }

                override fun newArray(size: Int): Array<PushData?> {
                        return arrayOfNulls(size)
                }
        }
}
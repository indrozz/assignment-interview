package com.base.project.ui.activity.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J*\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00030\bH&\u00a8\u0006\n"}, d2 = {"Lcom/base/project/ui/activity/login/SplashCallback;", "Lcom/base/project/ui/BaseCallback;", "gotoHomeActivity", "", "initFingerPrintAuth", "onVerified", "Lkotlin/Function0;", "onError", "Lkotlin/Function1;", "", "app_debug"})
public abstract interface SplashCallback extends com.base.project.ui.BaseCallback {
    
    public abstract void initFingerPrintAuth(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onVerified, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError);
    
    public abstract void gotoHomeActivity();
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        @java.lang.Override()
        public static void showAlertDialog(com.base.project.ui.activity.login.SplashCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.Object message, int positiveButton, int negativeButton, int neutralButton, boolean isCancelable, int purposeType, @org.jetbrains.annotations.Nullable()
        java.lang.Integer viewResId, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onPositiveButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNegativeButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNeutralButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onCustomAlertDialogCreated) {
        }
        
        @java.lang.Override()
        public static void dismissAlertDialog(com.base.project.ui.activity.login.SplashCallback $this) {
        }
        
        @java.lang.Override()
        public static void showMessage(com.base.project.ui.activity.login.SplashCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.String msg) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public static org.kodein.di.KodeinContext<?> getKodeinContext(com.base.project.ui.activity.login.SplashCallback $this) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public static org.kodein.di.KodeinTrigger getKodeinTrigger(com.base.project.ui.activity.login.SplashCallback $this) {
            return null;
        }
    }
}
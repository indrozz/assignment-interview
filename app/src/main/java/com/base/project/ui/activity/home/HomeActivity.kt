package com.base.project.ui.activity.home

import android.app.TimePickerDialog
import android.content.Intent
import android.widget.TimePicker
import com.base.project.R
import com.base.project.appdata.KEY_REMAINING_TIME_IN_SEC
import com.base.project.appdata.KEY_SELECTED_TIME
import com.base.project.databinding.ActivityHomeBinding
import com.base.project.service.AlarmNotifierService
import com.base.project.ui.activity.BaseActivity
import com.base.project.util.snackbarMessage
import java.util.*

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(
    R.layout.activity_home,
    HomeViewModel::class.java
), HomeCallback, TimePickerDialog.OnTimeSetListener {

    override fun bindViewModel(): Boolean {
        binding.homeViewModel = viewModel
        viewModel.callback = this
        return true
    }

    override fun openTimePicker() {
        val cal = Calendar.getInstance()
        TimePickerDialog(
            this,
            this,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            false
        ).show()
    }

    override fun checkAndStartAlarmNotifierService(remainTimeInSec : Int) {
        stopService(Intent(this, AlarmNotifierService :: class.java))
        startService(Intent(this, AlarmNotifierService :: class.java).apply {
            putExtra(KEY_REMAINING_TIME_IN_SEC, remainTimeInSec)
        })
    }

    override fun getSelectedTime() : String {
        val sharedPreferences = getSharedPreferences("db", MODE_PRIVATE)
        return sharedPreferences.getString(KEY_SELECTED_TIME, null) ?: getString(R.string.text_default_time)
    }

    override fun setSelectedTime(time: String) {
        val sharedPreferences = getSharedPreferences("db", MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(KEY_SELECTED_TIME, time)
        editor.apply()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        viewModel.selectedTime = "$hourOfDay : $minute"
        binding.tvSelectedTime.text = viewModel.selectedTime
        viewModel.onTimeSet(hourOfDay, minute)
    }

    override fun showMessage(msg: String) {
        snackbarMessage(msg)
    }





}

package com.base.project.util.adapter;

import java.lang.System;

/**
 * Developer : indrozz
 * Date : 09/03/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B%\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0007j\b\u0012\u0004\u0012\u00028\u0000`\b\u00a2\u0006\u0002\u0010\tJ\u0013\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\rJ(\u0010\u000e\u001a\u00020\u000b2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0007j\b\u0012\u0004\u0012\u00028\u0000`\b2\b\b\u0002\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u0012\u001a\u00020\u000bJ\b\u0010\u0013\u001a\u00020\u0005H\u0016J\u0016\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0007j\b\u0012\u0004\u0012\u00028\u0000`\bJ\u0006\u0010\u0015\u001a\u00020\u0011J\u0018\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0005H\u0016J\u0018\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0005H\u0016J\u0013\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\rJ\u0006\u0010\u001e\u001a\u00020\u000bJ\u001e\u0010\u001f\u001a\u00020\u000b2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0007j\b\u0012\u0004\u0012\u00028\u0000`\bJ%\u0010 \u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010!\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00020\u0005H&\u00a2\u0006\u0002\u0010\"R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0007j\b\u0012\u0004\u0012\u00028\u0000`\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/base/project/util/adapter/CustomAdapter;", "T", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/base/project/util/adapter/ItemViewHolder;", "layoutResId", "", "mList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "(ILjava/util/ArrayList;)V", "addItem", "", "item", "(Ljava/lang/Object;)V", "addMoreList", "list", "isClear", "", "clearList", "getItemCount", "getList", "isListEmpty", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeItem", "removeLastItem", "setList", "viewBinder", "objects", "(Lcom/base/project/util/adapter/ItemViewHolder;Ljava/lang/Object;I)V", "app_debug"})
public abstract class CustomAdapter<T extends java.lang.Object> extends androidx.recyclerview.widget.RecyclerView.Adapter<com.base.project.util.adapter.ItemViewHolder> {
    private final int layoutResId = 0;
    private java.util.ArrayList<T> mList;
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.base.project.util.adapter.ItemViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.base.project.util.adapter.ItemViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> list) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<T> getList() {
        return null;
    }
    
    public final void addMoreList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> list, boolean isClear) {
    }
    
    public final void clearList() {
    }
    
    public final void addItem(T item) {
    }
    
    public final void removeLastItem() {
    }
    
    public final void removeItem(T item) {
    }
    
    public final boolean isListEmpty() {
        return false;
    }
    
    public abstract void viewBinder(@org.jetbrains.annotations.NotNull()
    com.base.project.util.adapter.ItemViewHolder holder, T objects, int position);
    
    public CustomAdapter(int layoutResId, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> mList) {
        super();
    }
}
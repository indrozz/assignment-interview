package com.base.project.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0011\u001a\u00020\u000eH\u0002J\u000e\u0010\u0012\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tJ\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u000eH\u0016J\u0012\u0010\u0016\u001a\u00020\u000e2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u000e\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001bJ(\u0010\u001c\u001a\u00020\u00002\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00102\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000e0\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/base/project/util/FingerprintHelper;", "Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;", "()V", "ANDROID_KEY_STORE", "", "KEY_STORE_ALIAS", "cipher", "Ljavax/crypto/Cipher;", "context", "Landroid/content/Context;", "keyStore", "Ljava/security/KeyStore;", "onError", "Lkotlin/Function1;", "", "onVerified", "Lkotlin/Function0;", "generateKey", "init", "initCipher", "", "onAuthenticationFailed", "onAuthenticationSucceeded", "result", "Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;", "startAuth", "fm", "Landroid/hardware/fingerprint/FingerprintManager;", "with", "app_debug"})
public final class FingerprintHelper extends android.hardware.fingerprint.FingerprintManager.AuthenticationCallback {
    private static final java.lang.String ANDROID_KEY_STORE = "AndroidKeyStore";
    private static final java.lang.String KEY_STORE_ALIAS = "AndroidKey";
    private static android.content.Context context;
    private static kotlin.jvm.functions.Function0<kotlin.Unit> onVerified;
    private static kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError;
    private static javax.crypto.Cipher cipher;
    private static java.security.KeyStore keyStore;
    public static final com.base.project.util.FingerprintHelper INSTANCE = null;
    
    private final boolean initCipher() {
        return false;
    }
    
    private final void generateKey() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.base.project.util.FingerprintHelper init(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.base.project.util.FingerprintHelper with(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onVerified, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError) {
        return null;
    }
    
    public final void startAuth(@org.jetbrains.annotations.NotNull()
    android.hardware.fingerprint.FingerprintManager fm) {
    }
    
    @java.lang.Override()
    public void onAuthenticationSucceeded(@org.jetbrains.annotations.Nullable()
    android.hardware.fingerprint.FingerprintManager.AuthenticationResult result) {
    }
    
    @java.lang.Override()
    public void onAuthenticationFailed() {
    }
    
    private FingerprintHelper() {
        super();
    }
}
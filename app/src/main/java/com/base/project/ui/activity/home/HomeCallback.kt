package com.base.project.ui.activity.home

import com.base.project.ui.BaseCallback

interface HomeCallback : BaseCallback {

    fun openTimePicker()

    fun checkAndStartAlarmNotifierService(remainTimeInSec : Int)

    fun getSelectedTime() : String

    fun setSelectedTime(time : String)

}
package com.base.project.ui.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.base.project.R
import com.base.project.ui.BaseCallback
import com.base.project.ui.BaseViewModel
import org.kodein.di.android.kodein

abstract class BaseActivity<T : ViewDataBinding, U : BaseViewModel<out BaseCallback>>(
    private val layoutRes: Int,
    private val modelClass: Class<out BaseViewModel<out BaseCallback>>
) : AppCompatActivity(), BaseCallback {

    abstract fun bindViewModel() : Boolean

    protected lateinit var binding : T
    protected lateinit var viewModel : U

    override val mContext: Context
        get() = this

    override val kodein by kodein()

    override var mAlertDialog: AlertDialog? = null

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutRes)
        viewModel = ViewModelProvider(this).get(modelClass) as U
        bindViewModel()
        viewModel.onCreate(savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.onSaveInstanceState(outState)
    }

    fun overridingPendingExitTransition() {

        overridePendingTransition(
            R.anim.slide_from_left,
            R.anim.slide_to_right
        )
    }

    fun overridingPendingEntryTransition() {

        overridePendingTransition(
            R.anim.slide_from_right,
            R.anim.slide_to_left
        )
    }
}
package com.base.project.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0011\u001a\u00020\u0012J\u0012\u0010\u0013\u001a\u00020\u00122\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H&J\b\u0010\u0016\u001a\u00020\u0012H&J\b\u0010\u0017\u001a\u00020\u0012H&J\b\u0010\u0018\u001a\u00020\u0012H&J\u0010\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u0015H&R\u001c\u0010\u0007\u001a\u00028\u0000X\u0086.\u00a2\u0006\u0010\n\u0002\u0010\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0014\u0010\r\u001a\u00020\u000eX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001b"}, d2 = {"Lcom/base/project/ui/BaseViewModel;", "T", "Lcom/base/project/ui/BaseCallback;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "callback", "getCallback", "()Lcom/base/project/ui/BaseCallback;", "setCallback", "(Lcom/base/project/ui/BaseCallback;)V", "Lcom/base/project/ui/BaseCallback;", "repository", "Lcom/base/project/appdata/Repository;", "getRepository", "()Lcom/base/project/appdata/Repository;", "onBackPressed", "", "onCreate", "inState", "Landroid/os/Bundle;", "onDestroy", "onPause", "onResume", "onSaveInstanceState", "outState", "app_debug"})
public abstract class BaseViewModel<T extends com.base.project.ui.BaseCallback> extends androidx.lifecycle.AndroidViewModel {
    @org.jetbrains.annotations.NotNull()
    private final com.base.project.appdata.Repository repository = null;
    @org.jetbrains.annotations.NotNull()
    public T callback;
    
    @org.jetbrains.annotations.NotNull()
    protected final com.base.project.appdata.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final T getCallback() {
        return null;
    }
    
    public final void setCallback(@org.jetbrains.annotations.NotNull()
    T p0) {
    }
    
    public abstract void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle inState);
    
    public abstract void onResume();
    
    public abstract void onPause();
    
    public abstract void onDestroy();
    
    public abstract void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState);
    
    public final void onBackPressed() {
    }
    
    public BaseViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}
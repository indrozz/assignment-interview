package com.base.project.util.permission;

import java.lang.System;

/**
 * Developer : Indrajeet Gupta
 * Date : 14/07/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J!\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0002\u0010\u0011J+\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u000e\u0010\u0015\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f2\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J+\u0010\u0019\u001a\u00020\u00132\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u00042\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f\u00a2\u0006\u0002\u0010\u001aJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bJ+\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0014\u001a\u00020\u00042\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f\u00a2\u0006\u0002\u0010\u001fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/base/project/util/permission/Permission;", "", "()V", "REQUEST_PERMISSION_CODE", "", "alertDialog", "Landroid/app/AlertDialog;", "callback", "Lcom/base/project/util/permission/PermissionCallback;", "permissionMessage", "hasPermission", "", "activity", "Landroid/app/Activity;", "perms", "", "", "(Landroid/app/Activity;[Ljava/lang/String;)Z", "onRequestPermissionResult", "", "requestCode", "permissions", "grantResults", "", "(I[Ljava/lang/String;[I)V", "requestPermission", "(Landroid/app/Activity;I[Ljava/lang/String;)V", "setCallback", "showPermissionAlertDialog", "context", "Landroid/content/Context;", "(Landroid/content/Context;I[Ljava/lang/String;)V", "app_debug"})
public final class Permission {
    private static final int REQUEST_PERMISSION_CODE = 999;
    private static com.base.project.util.permission.PermissionCallback callback;
    private static int permissionMessage;
    private static android.app.AlertDialog alertDialog;
    public static final com.base.project.util.permission.Permission INSTANCE = null;
    
    public final boolean hasPermission(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String[] perms) {
        return false;
    }
    
    public final void requestPermission(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, int permissionMessage, @org.jetbrains.annotations.NotNull()
    java.lang.String[] perms) {
    }
    
    public final void onRequestPermissionResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.base.project.util.permission.Permission setCallback(@org.jetbrains.annotations.NotNull()
    com.base.project.util.permission.PermissionCallback callback) {
        return null;
    }
    
    public final void showPermissionAlertDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] perms) {
    }
    
    private Permission() {
        super();
    }
}
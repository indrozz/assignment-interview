package com.base.project.fcm;

import java.lang.System;

@kotlin.Suppress(names = {"CAST_NEVER_SUCCEEDS"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002J \u0010\b\u001a\u00020\t2\u000e\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\tH\u0002J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0018\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\tH\u0002J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0019\u001a\u00020\u001aH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/base/project/fcm/FCMService;", "Lcom/google/firebase/messaging/FirebaseMessagingService;", "()V", "notificationManager", "Landroid/app/NotificationManager;", "clearNotification", "", "dismissNotificationDelay", "getNotifyIntent", "Landroid/content/Intent;", "clazz", "Ljava/lang/Class;", "Landroid/app/Activity;", "pushData", "Lcom/base/project/fcm/PushData;", "handlePush", "notifyIntent", "handlePushWRTFlag", "makeNotification", "context", "Landroid/content/Context;", "onMessageReceived", "remoteMessage", "Lcom/google/firebase/messaging/RemoteMessage;", "onNewToken", "token", "", "Companion", "app_debug"})
public final class FCMService extends com.google.firebase.messaging.FirebaseMessagingService {
    private android.app.NotificationManager notificationManager;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHANNEL_ID = "niit_notification_channel_id";
    public static final int REQUEST_CODE = 2209;
    public static final long VIBRATION_PATTERN_VALUE = 1000L;
    public static final long COUNTER_TOTAL_TIME = 5000L;
    public static final long COUNTER_INTERVAL = 1000L;
    public static final com.base.project.fcm.FCMService.Companion Companion = null;
    
    @java.lang.Override()
    public void onNewToken(@org.jetbrains.annotations.NotNull()
    java.lang.String token) {
    }
    
    @java.lang.Override()
    public void onMessageReceived(@org.jetbrains.annotations.NotNull()
    com.google.firebase.messaging.RemoteMessage remoteMessage) {
    }
    
    private final void handlePushWRTFlag(com.base.project.fcm.PushData pushData) {
    }
    
    private final android.content.Intent getNotifyIntent(java.lang.Class<? extends android.app.Activity> clazz, com.base.project.fcm.PushData pushData) {
        return null;
    }
    
    private final void handlePush(android.content.Intent notifyIntent) {
    }
    
    private final void dismissNotificationDelay() {
    }
    
    private final void clearNotification() {
    }
    
    private final void makeNotification(android.content.Context context, android.content.Intent notifyIntent) {
    }
    
    public FCMService() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/base/project/fcm/FCMService$Companion;", "", "()V", "CHANNEL_ID", "", "COUNTER_INTERVAL", "", "COUNTER_TOTAL_TIME", "REQUEST_CODE", "", "VIBRATION_PATTERN_VALUE", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
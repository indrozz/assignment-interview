package com.base.project.util.adapter;

import java.lang.System;

/**
 * Developer : indrozz
 * Date : 07/02/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00020\u0003:\u0001\u0013B%\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0005j\b\u0012\u0004\u0012\u00028\u0000`\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\bH\u0016J \u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0005j\b\u0012\u0004\u0012\u00028\u0000`\u00062\u0006\u0010\u0011\u001a\u00020\u0012H&R\u001a\u0010\n\u001a\u000e\u0018\u00010\u000bR\b\u0012\u0004\u0012\u00028\u00000\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0005j\b\u0012\u0004\u0012\u00028\u0000`\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\f\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0005j\b\u0012\u0004\u0012\u00028\u0000`\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/base/project/util/adapter/FilterableAdapter;", "T", "Lcom/base/project/util/adapter/CustomAdapter;", "Landroid/widget/Filterable;", "mList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "layoutResId", "", "(Ljava/util/ArrayList;I)V", "customFilter", "Lcom/base/project/util/adapter/FilterableAdapter$CustomFilter;", "tempList", "getFilter", "Landroid/widget/Filter;", "getItemCount", "performFilteration", "filterationText", "", "CustomFilter", "app_debug"})
public abstract class FilterableAdapter<T extends java.lang.Object> extends com.base.project.util.adapter.CustomAdapter<T> implements android.widget.Filterable {
    private com.base.project.util.adapter.FilterableAdapter<T>.CustomFilter customFilter;
    private java.util.ArrayList<T> tempList;
    private java.util.ArrayList<T> mList;
    private final int layoutResId = 0;
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.util.ArrayList<T> performFilteration(@org.jetbrains.annotations.NotNull()
    java.lang.String filterationText);
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.widget.Filter getFilter() {
        return null;
    }
    
    public FilterableAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> mList, int layoutResId) {
        super(0, null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014J\u001c\u0010\u0007\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0014\u00a8\u0006\n"}, d2 = {"Lcom/base/project/util/adapter/FilterableAdapter$CustomFilter;", "Landroid/widget/Filter;", "(Lcom/base/project/util/adapter/FilterableAdapter;)V", "performFiltering", "Landroid/widget/Filter$FilterResults;", "constraint", "", "publishResults", "", "results", "app_debug"})
    final class CustomFilter extends android.widget.Filter {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        protected android.widget.Filter.FilterResults performFiltering(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence constraint) {
            return null;
        }
        
        @java.lang.Override()
        protected void publishResults(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence constraint, @org.jetbrains.annotations.Nullable()
        android.widget.Filter.FilterResults results) {
        }
        
        public CustomFilter() {
            super();
        }
    }
}
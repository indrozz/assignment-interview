package com.base.project.util.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

/**
 * Developer : Indrajeet Gupta
 * Date : 15/08/18.
 */

class ViewPagerAdapter(fm: FragmentManager) : androidx.fragment.app.FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var fragmentList : ArrayList<Fragment> = ArrayList()
    private var fragmentTitleList : ArrayList<String> = ArrayList()

    override fun getItem(position: Int): Fragment = fragmentList[position]

    override fun getCount(): Int = fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? = fragmentTitleList[position]

    fun addFragment(fragment: Fragment, title : String = "") {
        fragmentList.add(fragment)
        fragmentTitleList.add(title)
    }

    fun setFragmentList(fragmentList : ArrayList<Fragment>) {
        this.fragmentList = fragmentList
    }
    fun setFragmentTitleList(fragmentTitleList : ArrayList<String>) {
        this.fragmentTitleList = fragmentTitleList
    }

    fun getFragmentList() : ArrayList<Fragment> = this.fragmentList
}
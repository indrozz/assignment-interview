package com.base.project.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\f\u001a\u00020\rH\u0016J\u00ad\u0001\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0014\u001a\u00020\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u00122\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00122\u0014\b\u0002\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\r0\u001a2\u0014\b\u0002\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\r0\u001a2\u0014\b\u0002\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\r0\u001a2\u0014\b\u0002\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\r0\u001aH\u0016\u00a2\u0006\u0002\u0010\u001fJ\u0010\u0010 \u001a\u00020\r2\u0006\u0010!\u001a\u00020\"H\u0016R\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u00a8\u0006#"}, d2 = {"Lcom/base/project/ui/BaseCallback;", "Lorg/kodein/di/KodeinAware;", "mAlertDialog", "Landroidx/appcompat/app/AlertDialog;", "getMAlertDialog", "()Landroidx/appcompat/app/AlertDialog;", "setMAlertDialog", "(Landroidx/appcompat/app/AlertDialog;)V", "mContext", "Landroid/content/Context;", "getMContext", "()Landroid/content/Context;", "dismissAlertDialog", "", "showAlertDialog", "message", "", "positiveButton", "", "negativeButton", "neutralButton", "isCancelable", "", "purposeType", "viewResId", "onPositiveButtonClicked", "Lkotlin/Function1;", "onNegativeButtonClicked", "onNeutralButtonClicked", "onCustomAlertDialogCreated", "Landroid/view/View;", "(Ljava/lang/Object;IIIZILjava/lang/Integer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "showMessage", "msg", "", "app_debug"})
public abstract interface BaseCallback extends org.kodein.di.KodeinAware {
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.content.Context getMContext();
    
    @org.jetbrains.annotations.Nullable()
    public abstract androidx.appcompat.app.AlertDialog getMAlertDialog();
    
    public abstract void setMAlertDialog(@org.jetbrains.annotations.Nullable()
    androidx.appcompat.app.AlertDialog p0);
    
    public abstract void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.Object message, int positiveButton, int negativeButton, int neutralButton, boolean isCancelable, int purposeType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer viewResId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onPositiveButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNegativeButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNeutralButtonClicked, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onCustomAlertDialogCreated);
    
    public abstract void dismissAlertDialog();
    
    public abstract void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String msg);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        public static void showAlertDialog(com.base.project.ui.BaseCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.Object message, int positiveButton, int negativeButton, int neutralButton, boolean isCancelable, int purposeType, @org.jetbrains.annotations.Nullable()
        java.lang.Integer viewResId, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onPositiveButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNegativeButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onNeutralButtonClicked, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onCustomAlertDialogCreated) {
        }
        
        public static void dismissAlertDialog(com.base.project.ui.BaseCallback $this) {
        }
        
        public static void showMessage(com.base.project.ui.BaseCallback $this, @org.jetbrains.annotations.NotNull()
        java.lang.String msg) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public static org.kodein.di.KodeinContext<?> getKodeinContext(com.base.project.ui.BaseCallback $this) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public static org.kodein.di.KodeinTrigger getKodeinTrigger(com.base.project.ui.BaseCallback $this) {
            return null;
        }
    }
}
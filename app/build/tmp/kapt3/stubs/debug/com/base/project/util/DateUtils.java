package com.base.project.util;

import java.lang.System;

/**
 * Developer : Indrajeet Gupta
 * Date : 21/05/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015H\u0007J$\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0017\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015H\u0007J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0019J\u0018\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0013\u001a\u00020\u0004J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0017\u001a\u00020\u0004J\u001a\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015H\u0007J$\u0010 \u001a\u00020!2\u0006\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0017\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015H\u0007J\"\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u001d2\b\b\u0002\u0010\u001f\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015J\"\u0010$\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010\u001f\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"}, d2 = {"Lcom/base/project/util/DateUtils;", "", "()V", "DATE_TIME_BACKEND_FORMAT", "", "DATE_TIME_DATE_DAY_FORMAT", "DATE_TIME_DEFAULT_24_FORMAT", "DATE_TIME_DEFAULT_FORMAT", "DATE_TIME_FORMAT_1", "DATE_TIME_ONLY_DATE_FORMAT_1", "DATE_TIME_ONLY_DATE_FORMAT_2", "DATE_TIME_ONLY_DATE_FORMAT_3", "DATE_TIME_ONLY_DATE_FORMAT_4", "DATE_TIME_ONLY_TIME_24_FORMAT", "DATE_TIME_ONLY_TIME_FORMAT", "DATE_TIME_UTC_FORMAT", "convertDateTo", "dateTime", "fromFormat", "toFormat", "locale", "Ljava/util/Locale;", "convertToStandardFormat", "dateFormat", "convertUTCTimeInMillisToLocal", "", "timeInMillis", "convertUTCToLocal", "getCalendar", "Ljava/util/Calendar;", "getCurrentDateTime", "format", "getDate", "Ljava/util/Date;", "getDateOfFormat", "calendar", "getDateOfFormatFromMillis", "app_debug"})
public final class DateUtils {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_DEFAULT_FORMAT = "dd MMM, yyyy, hh:mm aa";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_FORMAT_1 = "yyyy-MM-dd, hh:mm aa";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_DEFAULT_24_FORMAT = "dd MMM, yyyy, HH:mm";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_DATE_FORMAT_1 = "dd MMM, yyyy";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_DATE_FORMAT_2 = "dd/MM/yyyy";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_DATE_FORMAT_3 = "yyyy-MM-dd";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_DATE_FORMAT_4 = "yyyyMMdd";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_TIME_FORMAT = "hh:mm aa";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_ONLY_TIME_24_FORMAT = "HH:mm";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_UTC_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_BACKEND_FORMAT = "yyyy-MM-dd HH:mm:ss";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_TIME_DATE_DAY_FORMAT = "EEE, dd/MM/yyyy";
    public static final com.base.project.util.DateUtils INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDateOfFormat(@org.jetbrains.annotations.NotNull()
    java.util.Calendar calendar, @org.jetbrains.annotations.NotNull()
    java.lang.String format, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public final java.lang.String getCurrentDateTime(@org.jetbrains.annotations.NotNull()
    java.lang.String format, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public final java.lang.String convertDateTo(@org.jetbrains.annotations.NotNull()
    java.lang.String dateTime, @org.jetbrains.annotations.NotNull()
    java.lang.String fromFormat, @org.jetbrains.annotations.NotNull()
    java.lang.String toFormat, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public final java.lang.String convertToStandardFormat(@org.jetbrains.annotations.NotNull()
    java.lang.String dateTime, @org.jetbrains.annotations.NotNull()
    java.lang.String dateFormat, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String convertUTCToLocal(@org.jetbrains.annotations.NotNull()
    java.lang.String dateTime, @org.jetbrains.annotations.NotNull()
    java.lang.String toFormat) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public final java.util.Date getDate(@org.jetbrains.annotations.NotNull()
    java.lang.String dateTime, @org.jetbrains.annotations.NotNull()
    java.lang.String dateFormat, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Calendar getCalendar(@org.jetbrains.annotations.NotNull()
    java.lang.String dateTime, @org.jetbrains.annotations.NotNull()
    java.lang.String dateFormat) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDateOfFormatFromMillis(long timeInMillis, @org.jetbrains.annotations.NotNull()
    java.lang.String format, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    public final long convertUTCTimeInMillisToLocal(long timeInMillis) {
        return 0L;
    }
    
    private DateUtils() {
        super();
    }
}
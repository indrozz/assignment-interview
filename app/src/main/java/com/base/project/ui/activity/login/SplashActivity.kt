package com.base.project.ui.activity.login

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.biometrics.BiometricPrompt
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import com.base.project.R
import com.base.project.databinding.ActivitySplashBinding
import com.base.project.ui.activity.BaseActivity
import com.base.project.ui.activity.home.HomeActivity
import com.base.project.util.FingerprintHelper
import com.base.project.util.permission.Permission
import com.base.project.util.snackbarMessage

@Suppress("DEPRECATION")
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(
    R.layout.activity_splash,
    SplashViewModel::class.java

), SplashCallback {

    private val cancellationSignal = CancellationSignal()
    private lateinit var fm : FingerprintManager

    override fun onCreate(savedInstanceState: Bundle?) {
        fm = getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
        super.onCreate(savedInstanceState)
    }

    private fun executeFingerPrintAuth(onVerified: () -> Unit, onError: (String) -> Unit) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

            val biometricPrompt = BiometricPrompt.Builder(this)
                .setTitle("Finger Print")
                .setSubtitle("Place your finger")
                .setNegativeButton(
                    "Cancel",
                    mainExecutor,
                    DialogInterface.OnClickListener { dialog, which -> onBackPressed()})
                .build()

            biometricPrompt.authenticate(
                cancellationSignal,
                mainExecutor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                        super.onAuthenticationSucceeded(result)
                        onVerified()
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        onError(getString(R.string.error_not_recognized))

                    }
                })

        } else {
            FingerprintHelper.init(this)
                .with(onVerified, onError)
                .startAuth(fm)
        }
    }

    override fun bindViewModel(): Boolean {
        binding.splashViewModel = viewModel
        viewModel.callback = this
        return true
    }

    override fun initFingerPrintAuth(onVerified: () -> Unit, onError: (String) -> Unit) {

        val km = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        if (!fm.isHardwareDetected()) {
            onError(getString(R.string.error_sensor_not_detected))

        } else if (!Permission.hasPermission(this, arrayOf(Manifest.permission.USE_FINGERPRINT))) {
            onError(getString(R.string.error_no_permission))

        } else if (!km.isKeyguardSecure) {
            onError(getString(R.string.error_no_lock))

        } else if (!fm.hasEnrolledFingerprints()) {
            onError(getString(R.string.error_no_finger_print))

        } else {
            executeFingerPrintAuth(onVerified, onError)
        }
    }

    override fun gotoHomeActivity() {
        startActivity(Intent(this, HomeActivity :: class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        })
    }

    override fun showMessage(msg: String) {
        snackbarMessage(msg)
    }
}

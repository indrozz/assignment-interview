package com.base.project.appdata

import android.content.Context
import androidx.lifecycle.LiveData
import com.base.project.network.ResponseHandler
import com.base.project.network.RestClient

class Repository(private val context: Context) {

    private val apiInterface = RestClient.getApiInterface()
    private val dataManager = DataManager.instance
    
}
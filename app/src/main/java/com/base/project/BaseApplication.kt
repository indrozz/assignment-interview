package com.base.project

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.base.project.appdata.DataManager
import com.base.project.appdata.Repository
import com.base.project.util.AppLog
import com.base.project.util.changeLanguage
import com.base.project.util.toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import io.paperdb.Paper
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class BaseApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@BaseApplication))
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        AppLog.init(applicationContext)
        Paper.init(applicationContext)
        if (DataManager.deviceToken != null) {
            initializeFirebaseMessagingServices { token ->
                DataManager.deviceToken = token
            }
        }
        changeLanguage(DataManager.defaultLanguage)
    }

    private fun initializeFirebaseMessagingServices(onTokenReceived : (String) -> Unit){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.v("TOKEN EXCEPTION => ", "Device Token Failed", task.exception)
                    return@OnCompleteListener
                }

                task.result?.token?.also {
                    Log.v("DEVICE TOKEN => ", it)
                    onTokenReceived(it)
                    toast(it)
                }
            })
    }
}
package com.base.project.ui.activity.login

import com.base.project.ui.BaseCallback

interface SplashCallback : BaseCallback {

    fun initFingerPrintAuth(onVerified : () -> Unit, onError : (String) -> Unit)

    fun gotoHomeActivity()

}
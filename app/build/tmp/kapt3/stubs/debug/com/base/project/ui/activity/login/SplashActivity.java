package com.base.project.ui.activity.login;

import java.lang.System;

@kotlin.Suppress(names = {"DEPRECATION"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\n\u001a\u00020\u000bH\u0016J*\u0010\f\u001a\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u000f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\r0\u0011H\u0002J\b\u0010\u0013\u001a\u00020\rH\u0016J*\u0010\u0014\u001a\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u000f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\r0\u0011H\u0016J\u0012\u0010\u0015\u001a\u00020\r2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\r2\u0006\u0010\u0019\u001a\u00020\u0012H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/base/project/ui/activity/login/SplashActivity;", "Lcom/base/project/ui/activity/BaseActivity;", "Lcom/base/project/databinding/ActivitySplashBinding;", "Lcom/base/project/ui/activity/login/SplashViewModel;", "Lcom/base/project/ui/activity/login/SplashCallback;", "()V", "cancellationSignal", "Landroid/os/CancellationSignal;", "fm", "Landroid/hardware/fingerprint/FingerprintManager;", "bindViewModel", "", "executeFingerPrintAuth", "", "onVerified", "Lkotlin/Function0;", "onError", "Lkotlin/Function1;", "", "gotoHomeActivity", "initFingerPrintAuth", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "showMessage", "msg", "app_debug"})
public final class SplashActivity extends com.base.project.ui.activity.BaseActivity<com.base.project.databinding.ActivitySplashBinding, com.base.project.ui.activity.login.SplashViewModel> implements com.base.project.ui.activity.login.SplashCallback {
    private final android.os.CancellationSignal cancellationSignal = null;
    private android.hardware.fingerprint.FingerprintManager fm;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void executeFingerPrintAuth(kotlin.jvm.functions.Function0<kotlin.Unit> onVerified, kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError) {
    }
    
    @java.lang.Override()
    public boolean bindViewModel() {
        return false;
    }
    
    @java.lang.Override()
    public void initFingerPrintAuth(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onVerified, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError) {
    }
    
    @java.lang.Override()
    public void gotoHomeActivity() {
    }
    
    @java.lang.Override()
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    public SplashActivity() {
        super(0, null);
    }
}
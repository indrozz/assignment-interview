package com.base.project.appdata

const val DEFAULT_LANGUAGE = "default_language"
const val LOG_FILE = "Log File"
const val LOG_FILE_NAME = "baseProject-log.txt"

const val DEVICE_TOKEN = "device_token"
const val IS_PUSH_NOTIFICATION_CALL = "is_push_notification_call"
const val PUSH_NOTIFICATION_DATA = "push_notification_data"

const val KEY_REMAINING_TIME_IN_SEC = "time_in_seconds"
const val KEY_SELECTED_TIME = "selected_time"


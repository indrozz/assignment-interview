package com.base.project;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001c\u0010\b\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\t0\u000bH\u0002J\b\u0010\r\u001a\u00020\tH\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/base/project/BaseApplication;", "Landroid/app/Application;", "Lorg/kodein/di/KodeinAware;", "()V", "kodein", "Lorg/kodein/di/LazyKodein;", "getKodein", "()Lorg/kodein/di/LazyKodein;", "initializeFirebaseMessagingServices", "", "onTokenReceived", "Lkotlin/Function1;", "", "onCreate", "app_debug"})
public final class BaseApplication extends android.app.Application implements org.kodein.di.KodeinAware {
    @org.jetbrains.annotations.NotNull()
    private final org.kodein.di.LazyKodein kodein = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.kodein.di.LazyKodein getKodein() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void initializeFirebaseMessagingServices(kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onTokenReceived) {
    }
    
    public BaseApplication() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public org.kodein.di.KodeinContext<?> getKodeinContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public org.kodein.di.KodeinTrigger getKodeinTrigger() {
        return null;
    }
}